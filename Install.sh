#!/bin/bash 

# Created by : Uniqueone 
# Gitlab : https://gitlab.com/uo_1
# Read the script before running it on your system.  

# INSTALL SCRIPT FOR FEDORA BASED DISTRIBUTION

# Install YUM MANUALLY 

# ENABLE THE NON-FREE REPOSITORY && FREE REPOSITORY MANUALLY 


sudo dnf install ranger git materia-gtk-theme rofi xfce4-power-manager xfce4-pulseaudio-plugin wget curl dconf-editor  flameshot && lxappearance gtk2-engines-murrine gtk2-engines htop  

# Install Playerctl 

sudo dnf install redhat-rpm-config playerctl 

# Install Pnmixer 

#wget  https://github.com/nicklan/pnmixer/releases/download/v0.7.2/pnmixer-v0.7.2.tar.gz 
#tar -zxvf pnmixer-v0.7.2.tar.gz

# Install PCMANFM 

#cd Downloads
#wget https://sourceforge.net/projects/pcmanfm/files/PCManFM%20%2B%20Libfm%20%28tarball%20release%29/PCManFM/pcmanfm-1.3.1.tar.xz/download 
#tar -xvf pcmanfm-1.3.1.tar.xz 


# Install Nautilus 

sudo dnf install nautilus 

# Install VolumeIcon
sudo dnf install volumeicon

# Install Spectrwm 

cd ~
sudo dnf install spectrwm
sudo touch ~/.spectrwm.conf
# cp /etc/spectrwm.conf ~/.spectrwm.conf 

# Install Nitrogen 

sudo dnf install nitrogen 

# Install Neofetch 

sudo dnf install neofetch 

# Install PSENSOR 

sudo dnf install lm_sensors lm_sensors-devel hddtemp psensor 

# Install BASHTOP && CMATRIX 
sudo dnf install bashtop && cmatrix 

# Install Brightnessctl 

sudo dnf install brightnessctl 

# Install Playerctl 

#sudo dnf install playerctl 



#!/bin/bash
# This is to set a wallpaper 
# Put this script in your window manager autostart file 
# 
# Make the script executable 

#feh --bg-scale ~/Pictures/BlackForest.jpg

feh --no-fehbg --bg-scale "$HOME/Pictures/BlackForest.jpg"

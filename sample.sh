#!/bin/bash

# Read the Documentation 

# INSTALL SCRIPT FOR DEBIAN BASED DISTROBUTION  

sudo aptitude install ranger materia-gtk-theme  rofi xfce4-power-manager xfce4-power-manager-data xfce4-power-manager-plugins   xfce4-pulseaudio-plugin  wget  arandr  dconf-editor  dconf-cli  flameshot &&  gtk2-engines-murrine  gtk2-engines 

# Insttall Spectrwm 

sudo aptitude install spectrwm 
sudo touch ~/.spectrwm.conf
# sudo cp /etc/spectrwm.conf  ~/.spectrwm.conf 

# Install Playerctl 

sudo aptitude install playerctl pnmixer

# Install Cmatrix && Neofetch 

sudo aptitude install neofetch && cmatrix 

# Install Lxappearance 

sudo aptitude install lxappearance 

# Install Brightnessctl 

sudo aptitude install brightnessctl



#!/bin/bash 

## Created by Uniqueone 
## Gitlab : https://gitlab.com/uo_1

## This is a script to install packages on arch base distribution.
 
## These are just some essential  packages that is needed after the base installation of arch linux or any other arch base distribution.  

## PLEASE READ THIS SCRIPT BEFORE RUNNING IT ON YOUR SYSTEM.  


## Wget  und Aria2                  # Aria2 command line  ## Reference :https://aria2.github.io/  
##sudo pacman -S --noconfirm wget aria2

## Sound 

## Reference : https://opensource.com/article/18/6/sound-themes-linux 
##             
##             https://yum-info.contradodigital.com/view-package/base/sound-theme-freedesktop/   


##sudo pacman -S alsa-utils alsa-firmware  pavucontrol playerctl audacious sound-theme-freedesktop gnome-sound-recorder  sof-firmware 


## VolumeIcon 
## Volume control for the system tray 

##sudo pacman -S volumeicon

## Brightness 

##sudo pacman -S brightnessctl 
##sudo brightnessctl set 70% 
## Bluetooth 

#sudo pacman -S bluez bluez-utils blueman && pulseaudio-bluetooth pulseaudio-bluez 

#sudo systemctl start bluetooth.service
#sudo systemctl enable bluetooth.service 

## News Feed 

## Reference :   https://lzone.de/liferea/ 
##               https://github.com/lwindolf/liferea
##               https://lzone.de/liferea/install.htm
##               https://newsboat.org/  
##               https://wiki.archlinux.org/title/Newsboat  

## NOTE  : NEWSBOAT is a command line rss/atom feed reader  

##sudo pacman -S liferea  
##sudo pacman -S newsboat 

## Andriod Tools / Support 

##sudo pacman -S android-tools  gvfs-mtp gvfs-gphoto2  gvfs 


## File Manager 

# sudo pacman -S pcmanfm
##sudo pacman -S nautilus 
##sudo pacman -S dconf-editor
##sudo pacman -S ranger 
##sudo pacman -S avahi xdg-user-dirs xdg-utils 
##gsettings set org.gnome.nautilus.preferences always-use-location-entry true


## Find / System Essential package      
## Note : you can only use one or the other between "mlocate" und "plocate"

# sudo pacman -S plocate 
# sudo pacman -S mlocate 

##sudo pacman -S tree                       ## Reference : https://www.geeksforgeeks.org/tree-command-unixlinux/
                                            ##     https://ostechnix.com/view-directory-tree-structure-linux/ 
## Password Manager

## Reference: https://sourceforge.net/projects/keepass/
##            
##            https://wiki.archlinux.org/title/KeePass

##sudo pacman -S keepass

## Video Player 

##sudo pacman -S smplayer
# sudo pacman -S parole

## Archive support (ZIP und UNZIP)

##sudo pacman -S zip 
##sudo pacman -S unzip 

## Application Launcher 

##sudo pacman -S rofi  
##sudo pacman -S dmenu 

## Browser 	# Choose your preferred browser 

##sudo pacman -S falkon
##sudo pacman -S chromium 

## Advanced Configuration and Power Interface (ACPI) 

## Reference : https://wiki.archlinux.org/index.php/ACPI_modules 
##	     : https://en.wikipedia.org/wiki/Advanced_Configuration_and_Power_Interface

## Note : acpi helps to give information about your battery status . 

## Run this command to display your battery percentage 

## acpi -i 

## Run the "man" command to get more information on how to use acpi 

##sudo pacman -S acpi 

## System Monitor 

##sudo pacman -S xfce4-power-manager psensor htop

## Polkit Setting 
# Reference : https://wiki.archlinux.org/index.php/Polkit

##sudo pacman -S mate-polkit 

## NTFS/MOUNT USB SUPPORT 

##sudo pacman -S ntfs-3g  nfs-utils udevil usbutils 

## Compositor 

## Note : You should uncomment the compositor that you wish to use or leave it as is , if you dont want any on your system. 

## Hint : Not using a compositor saves you on resources and uses less memory on your system. 

##sudo pacman -S picom 
# sudo pacman -S compton

## Wallpaper Setting 

##sudo pacman -S nitrogen feh 

## Image veiwer/Ueberzug    

## Note:  Ueberzug is a image viewer mainly used to display image in filemanger like ranger or vifm . 

##sudo pacman -S ueberzug viewnior 


## Themes / Lxappearance 

## Note : Lxappearance is used mainly to help set themes on your system. 

## Reference : https://github.com/PapirusDevelopmentTeam/papirus-icon-theme 

##sudo pacman -S lxappearance 
##sudo pacman -S ttf-hack
##sudo pacman -S ttf-roboto
##sudo pacman -S ttf-droid
##sudo pacman -S ttf-dejavu
##sudo pacman -S ttf-anonymous-pro
##sudo pacman -S terminus-font
##sudo pacman -S gtk-engine-murrine
##sudo pacman -S materia-gtk-theme
##sudo pacman -S ttf-font-awesome
##wget -qO- https://git.io/papirus-icon-theme-install | sh


## Qt5 Configuration Tool

## Reference :https://sourceforge.net/projects/qt5ct/ 

##sudo pacman -S qt5ct

## PDF READER 

## Reference : https://wiki.archlinux.org/index.php/Zathura 

## Reference :  https://github.com/pwmt/zathura	

## Reference : https://pwmt.org/projects/zathura/ 

## Reference : https://www.ubuntupit.com/linux-pdf-viewer-best-15-pdf-readers-reviewed-for-linux-users/ 

##sudo pacman -S zathura zathura-djvu zathura-pdf-poppler   
## sudo pacman -S evince
##sudo pacman -S qpdfview 

## Clipboard Support 

##sudo pacman -S xsel xclip 

## Network Support /Keyring

##sudo pacman -S  network-manager-applet 
##sudo pacman -S  gnome-keyring
##sudo pacman -S  nm-connection-editor
##sudo pacman -S  openbsd-netcat 
##sudo pacman -S  nmap 
##sudo pacman -S  wireshark-cli   wireshark-qt 
##sudo pacman -S  hashcat    hashcat-utils  hcxtools  hcxkeys  
##sudo pacman -S  john  
##sudo pacman -S firewalld

## Terminal Emulator 

# sudo pacman -S xterm
##sudo pacman -S terminator
##sudo pacman -S rxvt-unicode

## Text Editor 

## Reference : https://github.com/zyedidia/micro

#sudo pacman -S vim 
#sudo pacman -S nano 
##sudo pacman -S gedit 
##sudo pacman -S geany geany-plugins
##sudo pacman -S cherrytree 

## Note : "Curl" should be install on your system to run this script.   
# Note the curl script below is to install micro which is a text editor to replace nano. 
# curl https://getmic.ro | bash

# Install manaully 

# If your operating system does not have a binary release, but does run Go, you can build from source.

# Make sure that you have Go version 1.11 or greater and Go modules are enabled.

# git clone https://github.com/zyedidia/micro
# cd micro
# make build
# sudo mv micro /usr/local/bin # optional

## Notification tool 

## My default notification tool is zenity but you can replace with your preferred choice. 

##sudo pacman -S zenity
##sudo pacman -S xfce4-notifyd

## Screenshot Utility 

## Reference : https://flameshot.org 
##	       https://github.com/flameshot-org/flameshot

##sudo pacman -S flameshot 

## Arandr (Set Monitor) 

## Reference : https://christian.amsuess.com/tools/arandr/

##sudo pacman -S arandr


## Reference : https://computingforgeeks.com/install-kvm-qemu-virt-manager-arch-manjar/ 
## Reference : https://bbs.archlinux.org/viewtopic.php?id=176741   
## Virt-manager 

##sudo pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils 
##sudo pacman -S ebtables iptables
##sudo pacman -S libguestfs
##sudo systemctl enable libvirtd.service
##sudo systemctl start libvirtd.service
##sudo sed -i 's,#unix_sock_group = "libvirt",unix_sock_group = "libvirt",g' /etc/libvirt/libvirtd.conf 
##sudo sed -i 's,#unix_sock_rw_perms = "0770",unix_sock_rw_perms = "0770",g' /etc/libvirt/libvirtd.conf 
##sudo usermod -a -G libvirt $(whoami)
##newgrp libvirt
##sudo systemctl restart libvirtd.service 
# lsmod | grep tun                                      ## Note : Check to see if the module exist
##sudo modprobe tun                                       ## Note : I recieve errors about /dev/net/tun not found 


## Create folder  

# mkdir -p ~/{Downloads,Desktop,Pictures,Videos,AURHELPER,Documents,Templates,scripts}

mkdir -p ~/Downloads/ISO.Files

## Desktop/Window Manager 

## Spectrwm Website : https://github.com/conformal/spectrwm 
## Reference : https://wiki.archlinux.org/index.php/Spectrwm
## Reference : https://wiki.archlinux.org/index.php/Xfce
## Reference : https://wiki.archlinux.org/index.php/Bspwm
## Reference : https://github.com/baskerville/bspwm
## Reference : https://www.instructables.com/Bspwm-Installation-and-Configuration/
## Reference : https://docs.qtile.org/en/latest/
## Reference : https://wiki.archlinux.org/index.php/Qtile 

## Please note that "xorg" is needed if you wish to install a desktop/window manager. 

# sudo pacman -S xorg 
# sudo pacman -S xorg-server

# sudo pacman -S spectrwm		          # Hint : This is for Spectrwm window manager 
# touch ~/.spectrwm.conf		          # Hint : This is to create the configuration file 
# sudo cp /etc/spectrwm.conf  ~/.spectrwm.conf

# sudo pacman -S mate mate-extra 	# Hint : This is for Mate Desktop Environment 

# sudo pacman -S xfce4  xfce4-goodies	# Hint : This is for Xfce Desktop Environment 

sudo pacman -S bspwm 		# This is for bspwm (Binary Space Partitioning Window Manager )
sudo pacman -S sxhkd 		# This is what helps to setup keybinds on bspwm 


# sudo pacman -S qtile 		# This is for Qtile window manager writtten in python and configured in python . Useful for anyone that wants to learn or build on their python skills. 
# sudo pacman -S calcurse 	# Reference : https://calcurse.org/ 

## Note : "CALCURSE" is a command line calendar. 

## Lightdm Display Manager 

sudo pacman -S lightdm lightdm-gtk-greeter light-locker  lightdm-gtk-greeter-settings 
#sudo systemctl enable lightdm

## Note : Please reboot your machine manaully 

## LockScreen  / AUR HELPER     
## Note : My default aur helper is "pikaur" you can replace with your preferred choice.

## Note : My default lockscreen application is "i3lock-fancy" but you can replace with your preferred choice. 
## Dependencies 

sudo pacman -S awk bash coreutils imagemagick util-linux 

mkdir AURHELPER
cd AURHELPER
git clone https://aur.archlinux.org/i3lock-fancy.git.git
git clone https://aur.archlinux.org/pikaur.git 
cd i3lock-fancy
makepkg -si 
cd .. 
cd pikaur
makepkg -fsri 

## Dependencies to Install 
## Reference : https://github.com/polybar/polybar/wiki/Compiling 


sudo pacman -S --noconfirm clang cppcheck  xcb-util-image  xcb-util-wm  xcb-proto  libxcb  cairo python-cairo  python-cairocffi 

## Build polybar from source 

cd AURHELPER
aria2c -x 4 -s 4 https://github.com/polybar/polybar/releases/download/3.5.7/polybar-3.5.7.tar.gz
tar xvzf polybar-3.5.7.tar.gz
cd polybar
mkdir build
cd build
cmake -DCMAKE_CXX_COMPILER="clang++" ..
make -j$(nproc)
sudo make install


# Install powerlevel10k
# Reference : https://github.com/romkatv/powerlevel10k
# Setting up zsh Shell 

sudo pacman -S zsh zsh-completions 
chsh -s /bin/zsh # (relogin to activate) 
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" 
mkdir .local/share/fonts
cd .local/share/fonts
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Regular.ttf 
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Italic.ttf
wget https://github.com/romkatv/powerlevel10k-media/raw/master/MesloLGS%20NF%20Bold%20Italic.ttf
fc-cache -v
git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k 

git clone https://github.com/zsh-users/zsh-autosuggestions.git $ZSH_CUSTOM/plugins/zsh-autosuggestions
git clone https://github.com/zsh-users/zsh-syntax-highlighting.git $ZSH_CUSTOM/plugins/zsh-syntax-highlighting 


## Add this to your .zshrc file 

## In between plugins "zsh-syntax-highlighting"
## source ~/powerlevel10k/powerlevel10k.zsh-theme
## source $ZSH_CUSTOM/plugins/zsh-syntax-highlighting 



## Check the packages that are install manually 

# pacman -Qm 		# Check AUR OR FORIEGN packages install  
pacman -Qm  >>  Aurpackages.txt  
# pacman -Qn 		# Check packages install 
pacman -Qn >> package.list	# Packages store in a file 

# cat package.list 		# View the file 
